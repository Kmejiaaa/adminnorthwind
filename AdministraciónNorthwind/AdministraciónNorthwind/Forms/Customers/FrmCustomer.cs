﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdministraciónNorthwind.DAO;
using AdministraciónNorthwind.Model;

namespace AdministraciónNorthwind
{
    public partial class FrmCustomer : Form
    {
        bool agregar = false;
        bool modificar = false;
        private string[] Columns = new[] { "Customer ID", "Company Name", "Contact Name", "Contact Title", "Address",
        "City","Region","PostalCode","Country","Phone","Fax"};
        DaoCustomers daoCustomers = new DaoCustomers();

        public FrmCustomer()
        {
            InitializeComponent();
            cargarDatos();
        }

        #region

        public void cargarDatos()
        {
            List<Customers> customers = new List<Customers>();
            try
            {
                //Recupera una lista de la base de datos de todos los customers
                customers = daoCustomers.customersList();
                //limpiar valores del DataGridView
                dgvDatos.Columns.Clear();
                //Asignacion de valores al DataGridView
                dgvDatos.DataSource = customers.Select(x => new
                {
                    x.CustomerID,
                    x.CompanyName,
                    x.ContactName,
                    x.ContactTitle,
                    x.Address,
                    x.City,
                    x.Region,
                    x.PostalCode,
                    x.Country,
                    x.Phone,
                    x.Fax
                }).ToList();

                for (int i = 0; i < Columns.Count(); i++)
                {
                    dgvDatos.Columns[i].HeaderText = Columns[i];
                }

                if (dgvDatos.RowCount > 0)
                {
                    dgvDatos.Rows[0].Selected = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente" + ex.Message);
            }
        }

        public void llenarDatos()
        {
            if (dgvDatos.RowCount > 0)
            {
                txtId.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[0].Value.ToString();
                txtCompany.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[1].Value.ToString();
                txtContact.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[2].Value.ToString();
                txtTitle.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[3].Value.ToString();
                txtAddress.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[4].Value.ToString();
                txtCity.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[5].Value.ToString();
                if (dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[6].Value != null)
                {
                    txtRegion.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[6].Value.ToString();
                }
                txtPCode.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[7].Value.ToString();
                txtCountry.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[8].Value.ToString();
                txtPhone.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[9].Value.ToString();
                if (dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[10].Value != null)
                {
                    txtFax.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[10].Value.ToString();
                }
                
            }
        }

        public void habilitarCampos(bool i)
        {
            txtCompany.Enabled=i;
            txtContact.Enabled = i;
            txtTitle.Enabled = i;
            txtAddress.Enabled = i;
            txtCity.Enabled=i;
            txtRegion.Enabled = i;
            txtPCode.Enabled = i;
            txtCountry.Enabled=i;
            txtPhone.Enabled = i;
            txtFax.Enabled = i;
        }

        public Customers setearValores()
        {
            try
            {
                Customers customer = new Customers();
                customer.CustomerID = txtId.Text;
                customer.CompanyName = txtCompany.Text;
                customer.ContactName = txtContact.Text;
                customer.ContactTitle = txtTitle.Text;
                customer.Address = txtAddress.Text;
                customer.City = txtCity.Text;
                customer.Region = txtRegion.Text;
                customer.PostalCode = txtPCode.Text;
                customer.Country = txtCountry.Text;
                customer.Phone = txtPhone.Text;
                customer.Fax = customer.Fax;

                return customer;

            }
            catch (Exception ex)
            {
                return new Customers();
            }
        }

        public bool validarCampos()
        {
            bool estado = false;

            if (!txtId.Text.Equals("") && !txtCompany.Text.Equals("") && !txtContact.Text.Equals("") && !txtTitle.Text.Equals("") && !txtAddress.Text.Equals("") && !txtCity.Text.Equals("") && !txtRegion.Text.Equals("") && !txtPCode.Text.Equals("") && !txtCountry.Text.Equals("") && !txtPhone.Text.Equals("") && !txtFax.Text.Equals(""))
            {
                estado = true;
            }
            else
            {
                estado = false;
            }

            return estado;
        }

        public void limpiar()
        {
            txtId.Clear();
            txtCompany.Clear();
            txtContact.Clear();
            txtTitle.Clear();
            txtAddress.Clear();
            txtCity.Clear();
            txtRegion.Clear();
            txtPCode.Clear();
            txtCountry.Clear();
            txtPhone.Clear();
            txtFax.Clear();
        }

        public void estadoBotones(string texto)
        {
            if (texto == "Agregar")
            {
                txtId.Enabled = true;
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;


            }
            else if (texto == "Modificar")
            {
                txtId.Enabled = false;
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
            }
            else
            {
                txtId.Enabled = false;
                btnGuardar.Enabled = false;
                btnEliminar.Enabled = true;
                btnModificar.Enabled = true;
                btnAgregar.Enabled = true;
            }
        }
        #endregion


        private void Form1_Load(object sender, EventArgs e)
        {
            estadoBotones("Nuevo");
            habilitarCampos(false);
            limpiar();
            if (dgvDatos.RowCount > 0)
            {
                dgvDatos.Rows[0].Selected = false;

            }
        }

        private void dgvDatos_Click(object sender, EventArgs e)
        {
            llenarDatos();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            agregar = true;
            modificar = false;
            estadoBotones("Agregar");
            limpiar();
            habilitarCampos(true);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (validarCampos())
            {
                if (agregar == true)
                {
                    if (daoCustomers.insert(setearValores()))
                    MessageBox.Show("Información registrado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                    MessageBox.Show("Error al registrar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    agregar = false;
                    modificar = false;
                }
                else if (modificar == true)
                {
                    if (daoCustomers.modify(setearValores(),txtId.Text))
                        MessageBox.Show("Información registrado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al registrar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    agregar = false;
                    modificar = false;
                }

                limpiar();
                cargarDatos();
                habilitarCampos(false);
                estadoBotones("Nuevo");
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (dgvDatos.SelectedRows.Count > 0)
            {
                agregar = false;
                modificar = true;
                habilitarCampos(true);
                estadoBotones("Modificar");
            }
            else
                MessageBox.Show("Debe seleccionar un registro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvDatos.SelectedRows.Count > 0)
            {
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    if (daoCustomers.delete(txtId.Text))
                        MessageBox.Show("Registro eliminado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al Eliminar la información", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("Se ha omitido la eliminacion del registro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                limpiar();
                cargarDatos();
            }
            else
                MessageBox.Show("Debe seleccionar un registro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            habilitarCampos(false);
            cargarDatos();
            estadoBotones("Nuevo");
        }


        private void txtId_KeyPress(object sender, KeyPressEventArgs e)
        {

            if(txtId.TextLength==5)
            {
                if (Char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
            else
            {
                if (Char.IsLetter(e.KeyChar))
                {
                    e.Handled = false;
                }
                else if (Char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void txtContact_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtTitle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtCountry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtCity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtRegion_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        
    }
}
